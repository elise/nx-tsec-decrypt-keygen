# NX TSEC Decrypt Keygen

A tool to decrypt the keygen blob from the NX TSEC firmware.

## Installation

### Prerequesites

* [Rust](https://rustup.rs)

```sh
cargo install --git https://gitlab.com/elise/nx-tsec-decrypt-keygen.git
```

## Usage

```sh
nx-tsec-decrypt-keygen <input_path> <output_path> <key>
```
