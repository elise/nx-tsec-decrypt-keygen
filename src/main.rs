use std::path::PathBuf;

use clap::Parser;
use libaes::Cipher;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("File not found: {0}")]
    FileNotFound(PathBuf),
    #[error(transparent)]
    Io(#[from] std::io::Error),
    #[error("Invalid key format, excepted 32 characters of A-Z,0-9")]
    BadKey,
}

#[derive(Debug, Parser)]
#[clap(author, version, about, long_about = None)]

pub struct Args {
    /// Input binary
    pub input_path: PathBuf,
    /// Output path
    pub out_path: PathBuf,
    /// Hex encoded AES key (32 chars of A-Z,0-9)
    pub key: String,
}

fn actual_main() -> Result<(), Error> {
    let args = Args::parse();

    if !args.input_path.is_file() {
        return Err(Error::FileNotFound(args.input_path));
    }

    if args.key.len() != 32 {
        return Err(Error::BadKey);
    }

    let decoded_key = hex::decode(args.key.as_str()).map_err(|_| Error::BadKey)?;
    let key: [u8; 0x10] = decoded_key.try_into().map_err(|_| Error::BadKey)?;
    let iv = [0u8; 0x10];

    let data = std::fs::read(args.input_path.as_path())?;

    let output = Cipher::new_128(&key).cbc_decrypt(&iv, &data);

    std::fs::write(args.out_path.as_path(), output)?;

    Ok(())
}

fn main() {
    if let Err(why) = actual_main() {
        eprintln!("{}", why);
        std::process::exit(1);
    }
}
